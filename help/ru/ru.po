# Russian translation for gnome-tetravex.
# Copyright (C) 2022 gnome-tetravex's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-tetravex package.
# Ser82-png <sw@atrus.ru>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-tetravex master\n"
"POT-Creation-Date: 2021-12-23 04:31+0000\n"
"PO-Revision-Date: 2022-01-16 18:45+1000\n"
"Language-Team: Russian <gnome-cyr@gnome.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ru\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"Last-Translator: Ser82-png\n"
"X-Generator: Poedit 2.3\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "Ser82-png <sw@atrus.ru>, 2022"

#. (itstool) path: info/desc
#: C/hint.page:7
msgid "Solve the game automatically."
msgstr "Решить игру автоматические."

#. (itstool) path: credit/name
#: C/hint.page:10 C/index.page:17 C/move.page:12 C/size.page:12 C/usage.page:12
#: C/winning.page:12
msgid "Milo Casagrande"
msgstr "Milo Casagrande"

#. (itstool) path: page/title
#: C/hint.page:14
msgid "Hints"
msgstr "Подсказки"

#. (itstool) path: note/p
#: C/hint.page:17
msgid ""
"If you use the following method to complete the game, your score will not be "
"included among the high scores."
msgstr ""
"Если вы используете для завершения игры следующий метод, то ваш результат не "
"будет включён в число лучших результатов."

#. (itstool) path: section/title
#: C/hint.page:24
msgid "Solve the game"
msgstr "Решить игру"

#. (itstool) path: section/p
#: C/hint.page:25
msgid ""
"If you get stuck in the game and want to solve it automatically, press the "
"question mark button in the bottom-right corner."
msgstr ""
"Если вы застряли в игре и хотите решить её автоматически, нажмите кнопку со "
"знаком вопроса в правом нижнем углу."

#. (itstool) path: info/title
#: C/index.page:8
msgctxt "link"
msgid "GNOME Tetravex"
msgstr "GNOME Тетравекс"

#. (itstool) path: info/title
#: C/index.page:9
msgctxt "text"
msgid "GNOME Tetravex"
msgstr "GNOME Тетравекс"

#. (itstool) path: info/desc
#: C/index.page:10
msgid "GNOME Tetravex help."
msgstr "Справка по GNOME Тетравекс."

#. (itstool) path: credit/name
#: C/index.page:13 C/usage.page:16
msgid "Rob Bradford"
msgstr "Rob Bradford"

#. (itstool) path: license/p
#: C/index.page:21 C/move.page:16 C/size.page:16 C/usage.page:20
#: C/winning.page:16
msgid "Creative Commons Share Alike 3.0"
msgstr "Creative Commons Share Alike 3.0"

#. (itstool) path: media/span
#: C/index.page:26
msgid "GNOME Tetravex logo"
msgstr "Логотип GNOME Тетравекс"

#. (itstool) path: page/title
#: C/index.page:25
msgid "<_:media-1/> GNOME Tetravex"
msgstr "<_:media-1/> GNOME Тетравекс"

#. (itstool) path: page/p
#: C/index.page:30
msgid ""
"<app>GNOME Tetravex</app> is a simple puzzle game which scope is to move the "
"various pieces from one side of the board to the other side, so that the "
"same numbers depicted on the pieces are touching each other."
msgstr ""
"<app>GNOME Тетравекс</app> — это простая игра-головоломка, цель которой — "
"переместить различные плитки с одной стороны доски на другую так, чтобы "
"одинаковые числа, изображённые на сторонах плиток, соприкасались друг с "
"другом."

#. (itstool) path: page/p
#: C/index.page:35
msgid ""
"The game is timed, and these times are used as the score for the game. You "
"have to move all the pieces in the least time possible."
msgstr ""
"Игра рассчитана на время, и оно используется для начисления очков. Вы должны "
"переместить все плитки за наименьшее время."

#. (itstool) path: section/title
#: C/index.page:41
msgid "Basic Gameplay &amp; Usage"
msgstr "Базовый геймплей &amp; Основы"

#. (itstool) path: section/title
#: C/index.page:45
msgid "Tips &amp; Tricks"
msgstr "Советы &amp; Трюки"

#. (itstool) path: info/desc
#: C/license.page:8
msgid "Legal information."
msgstr "Правовая информация."

#. (itstool) path: page/title
#: C/license.page:11
msgid "License"
msgstr "Лицензия"

#. (itstool) path: page/p
#: C/license.page:12
msgid ""
"This work is distributed under a CreativeCommons Attribution-Share Alike 3.0 "
"Unported license."
msgstr ""
"Эта работа распространяется под лицензией CreativeCommons Attribution-Share "
"Alike 3.0 Unported."

#. (itstool) path: page/p
#: C/license.page:20
msgid "You are free:"
msgstr "Вы свободны:"

#. (itstool) path: item/title
#: C/license.page:25
msgid "<em>To share</em>"
msgstr "<em>Делиться</em>"

#. (itstool) path: item/p
#: C/license.page:26
msgid "To copy, distribute and transmit the work."
msgstr "Копировать, распространять и передавать эту работу."

#. (itstool) path: item/title
#: C/license.page:29
msgid "<em>To remix</em>"
msgstr "<em>Изменять</em>"

#. (itstool) path: item/p
#: C/license.page:30
msgid "To adapt the work."
msgstr "Адаптировать эту работу."

#. (itstool) path: page/p
#: C/license.page:33
msgid "Under the following conditions:"
msgstr "При следующих условиях:"

#. (itstool) path: item/title
#: C/license.page:38
msgid "<em>Attribution</em>"
msgstr "<em>Ссылка на источник</em>"

#. (itstool) path: item/p
#: C/license.page:39
msgid ""
"You must attribute the work in the manner specified by the author or "
"licensor (but not in any way that suggests that they endorse you or your use "
"of the work)."
msgstr ""
"Вы должны указать авторство работы в порядке, указанном автором или "
"лицензиатом (но не таким образом, что они поддерживают вас или использование "
"вами работы)."

#. (itstool) path: item/title
#: C/license.page:46
msgid "<em>Share Alike</em>"
msgstr "<em>Share Alike</em>"

#. (itstool) path: item/p
#: C/license.page:47
msgid ""
"If you alter, transform, or build upon this work, you may distribute the "
"resulting work only under the same, similar or a compatible license."
msgstr ""
"Если вы изменяете, трансформируете или берёте за основу данную работу, вы "
"можете распространять полученное произведение только по такой же, подобной "
"или совместимой лицензии."

#. (itstool) path: page/p
#: C/license.page:53
msgid ""
"For the full text of the license, see the <link href=\"http://"
"creativecommons.org/licenses/by-sa/3.0/legalcode\">CreativeCommons website</"
"link>, or read the full <link href=\"http://creativecommons.org/licenses/by-"
"sa/3.0/\">Commons Deed</link>."
msgstr ""
"Полный текст лицензии смотрите на <link href=\"http://creativecommons.org/"
"licenses/by-sa/3.0/legalcode\">веб-сайте CreativeCommons</link>, или "
"прочитайте полный текст <link href=\"http://creativecommons.org/licenses/by-"
"sa/3.0/\">Commons Deed</link>."

#. (itstool) path: info/desc
#: C/move.page:8
msgid "How to move the pieces on the game board."
msgstr "Как перемещать плитки на игровой доске."

#. (itstool) path: page/title
#: C/move.page:20
msgid "Move the pieces"
msgstr "Перемещение плиток"

#. (itstool) path: section/title
#: C/move.page:23
msgid "Move a piece"
msgstr "Переместить плитку"

#. (itstool) path: section/p
#: C/move.page:24
msgid ""
"To move a piece, you can either click on it to select it, and click on the "
"square to place it or drag it to the new square."
msgstr ""
"Чтобы переместить плитку, вы можете щёлкнуть по ней мышкой, чтобы выбрать, а "
"затем щёлкнуть по квадрату, куда собираетесь её поставить, либо просто "
"перетащить её мышью на новое место."

#. (itstool) path: section/p
#: C/move.page:27
msgid "These moves are not permanent and can be reversed."
msgstr "Эти перемещения не являются постоянными и могут быть отменены."

#. (itstool) path: note/p
#: C/move.page:31
msgid ""
"The pieces in the left box can be moved all at once in one direction. Press "
"the menu button in the top-right corner of the window and select <gui style="
"\"menuitem\">Keyboard Shortcuts</gui> for more information."
msgstr ""
"Плитки на левой доске можно перемещать все сразу в одном направлении. "
"Нажмите кнопку меню в правом верхнем углу окна и выберите <gui style="
"\"menuitem\">Комбинации клавиш</gui> для получения дополнительной информации."

#. (itstool) path: info/desc
#: C/size.page:7
msgid "Change the size of the game board."
msgstr "Изменить размер игровой доски."

#. (itstool) path: page/title
#: C/size.page:19
msgid "Game board size"
msgstr "Размер игровой доски"

#. (itstool) path: page/p
#: C/size.page:21
msgid ""
"To change the size of the game board, in order to increase or decrease the "
"complexity of the game, press the menu button in the top-right corner of the "
"window and select <gui style=\"menuitem\">Size</gui>, then select the "
"dimension of the board."
msgstr ""
"Чтобы изменить размер игровой доски, и поменять сложность игры, нажмите "
"кнопку меню в правом верхнем углу окна и выберите <gui style=\"menuitem"
"\">Размер</gui>, затем выберите необходимый размер доски."

#. (itstool) path: page/p
#: C/size.page:27
msgid ""
"You can select from five different dimension, from 2×2 to 6×6. The default "
"one is 3×3."
msgstr ""
"Вы можете выбрать один из пяти установленных размеров: от 2×2 до 6×6. По "
"умолчанию это 3×3."

#. (itstool) path: note/p
#: C/size.page:31
msgid ""
"If the pieces are too small, resizing the window will change their size."
msgstr "Если плитки слишком малы, изменение размера окна изменит и их размер."

#. (itstool) path: info/desc
#: C/usage.page:9
msgid "How to play <app>GNOME Tetravex</app>."
msgstr "Как играть в <app>GNOME Тетравекс</app>."

#. (itstool) path: page/title
#: C/usage.page:24
msgid "Play <app>GNOME Tetravex</app>"
msgstr "Играть в <app>GNOME Тетравекс</app>"

#. (itstool) path: section/title
#: C/usage.page:27
msgid "Basic usage"
msgstr "Основы игры"

#. (itstool) path: section/p
#: C/usage.page:28
msgid ""
"The game window is divided into two boxes: the one on the right side of the "
"window contains the pieces that needs to be arranged; the one on the left "
"side of the window is where the pieces from the right box will be arranged "
"and moved. Each piece is divided into four little triangles containing a "
"number."
msgstr ""
"Игровое окно разделено на две части: в правой части располагаются плитки, "
"которые нужно расставить; в левой части следует расположить плитки, "
"перемещённые с правой. Каждая плитка разделена на четыре маленьких "
"треугольника, содержащих число."

#. (itstool) path: section/p
#: C/usage.page:35
msgid ""
"The scope of the game is to position the pieces so that only two identical "
"numbers are next to each other."
msgstr ""
"Суть игры - расположить плитки так, чтобы в соседних треугольниках были "
"записаны одинаковые числа."

#. (itstool) path: section/p
#: C/usage.page:39
msgid ""
"It is not possible to position the pieces so that different number are next "
"to each other."
msgstr ""
"Невозможно расположить плитки так, чтобы соприкасаемые стороны их не "
"совпадали."

#. (itstool) path: section/p
#: C/usage.page:43
msgid ""
"The game is completed when you have positioned all the pieces in their right "
"positions."
msgstr "Игра будет завершена, когда вы разместите все плитки на своих местах."

#. (itstool) path: note/p
#: C/usage.page:48
msgid ""
"When you start <app>GNOME Tetravex</app>, the game will start immediately, "
"and the time will start counting."
msgstr ""
"Когда вы запустите <app>GNOME Тетравекс</app>, немедленно запустится игра, и "
"начнется отсчёт времени."

#. (itstool) path: section/title
#: C/usage.page:57
msgid "Video demonstration"
msgstr "Видеодемонстрация"

#. (itstool) path: section/p
#: C/usage.page:58
msgid ""
"This short video shows how the game works, and what are the possible "
"movements."
msgstr ""
"В этом коротком видео показано, как работает игра и какие возможны движения."

#. (itstool) path: media/p
#: C/usage.page:62
msgid "Demo"
msgstr "Демо"

#. (itstool) path: div/p
#: C/usage.page:66
msgid ""
"Drag pieces from the right to the left, making sure that adjacent edges have "
"the same number and color."
msgstr ""
"Перетаскивайте плитки справа налево, следя за тем, чтобы соприкасающиеся "
"стороны имели одинаковое число и цвет."

#. (itstool) path: div/p
#: C/usage.page:70
msgid ""
"Hold down <key>Ctrl</key> and press the arrow keys to move all placed pieces "
"at once."
msgstr ""
"Удерживая нажатой клавишу <key>Ctrl</key>, нажмите клавишу со стрелкой, "
"чтобы переместить все установленные плитки одновременно."

#. (itstool) path: div/p
#: C/usage.page:74
msgid "Continue dragging pieces until they all fit together on the left."
msgstr ""
"Продолжайте перемещать плитки, пока не соберёте их все вместе на левой доске."

#. (itstool) path: info/desc
#: C/winning.page:9
msgid "Simple tips on how to possibly win a game."
msgstr "Простые советы о том, как возможно выиграть игру."

#. (itstool) path: page/title
#: C/winning.page:20
msgid "Win <app>GNOME Tetravex</app>"
msgstr "Победа в <app>GNOME Тетравекс</app>"

#. (itstool) path: note/p
#: C/winning.page:23
msgid ""
"The following tips will not assure you to win a game, they are intended as a "
"simple aid."
msgstr ""
"Следующие советы не гарантируют вам победу в игре, они предназначены только "
"для помощи."

#. (itstool) path: section/title
#: C/winning.page:30
msgid "Look for a single number"
msgstr "Ищите одиночное число"

#. (itstool) path: section/p
#: C/winning.page:31
msgid ""
"When you start a new game, look for a single number, or a number that has no "
"similar numbers that can be placed next to it."
msgstr ""
"В начале игры ищите на плитках число, которое является уникальным, или "
"число, которое не может иметь соседних плиток с таким же значением."

#. (itstool) path: section/p
#: C/winning.page:35
msgid ""
"If you have one such number, that piece is a valid starting piece, since you "
"know where it has to be placed."
msgstr ""
"Если вы нашли такое число, можно начать с плитки, которая его содержит, "
"поскольку можно точно определить, где следует расположить эту плитку."

#. (itstool) path: section/p
#: C/winning.page:39
msgid ""
"Note that the bigger the size of the board, the harder is to find such a "
"number."
msgstr ""
"Обратите внимание, что чем больше размер доски, тем сложнее найти такое "
"число."

#. (itstool) path: section/title
#: C/winning.page:46
msgid "Have a tip to share?"
msgstr "Есть совет, которым можно поделиться?"

#. (itstool) path: section/p
#: C/winning.page:47
msgid ""
"If you have a tip to share about <app>GNOME Tetravex</app>, or any other "
"GNOME games, write us at gnome-doc-list@gnome.org. We will add them here!"
msgstr ""
"Если у вас есть совет по поводу <app>GNOME Тетравекс</app> или любых других "
"игр для GNOME, напишите нам по адресу gnome-doc-list@gnome.org. Мы добавим "
"их сюда!"
